<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/produtos', 'ProdutoController@lista');
Route::get('/', 'ProdutoController@lista');

// É obrigatório ter um id e esse id tem que ser número 
Route::get(
        '/produtos/mostra/{id}', 'ProdutoController@mostra'
)->where('id', '[0-9]+');

// Não é obrigatório ter um id
//Route::get('/produtos/mostra/{id?}', 'ProdutoController@mostra');
Route::get('/produtos/novo', 'ProdutoController@novo');
Route::post('produtos/adiciona', 'ProdutoController@adiciona');

