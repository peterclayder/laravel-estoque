<?php

namespace estoque\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    public function lista()
    {
        $produtos = DB::select('select * from produtos');
        // verifica se a view existe
        if (view()->exists("produtos.listagem")) {
            /**
             * Carrega a view e passa o conteudo da variável $produtos
             */
            //return view("listagem")->with("produtos", $produtos);
            return view("produtos.listagem", ["produtos" => $produtos]);

        }
    }
    public function mostra(Request $request)
    {
        $id = $request->route("id");
        $resposta = DB::select('select * from back_produtos where id = ?', [$id]);
        if (empty($resposta)) {
            echo "nada encontrado";
        } else {
            return view("produtos.mostra", ['p' => $resposta[0]]);
        }


    }
    public function novo(){
        return view('produtos.formulario');
    }
    public function adiciona(Request $request){
        $nome = $request->input("nome");
        $valor = $request->input("valor");
        $descricao = $request->input("descricao");
        $quantidade = $request->input("quantidade");
        DB::insert('insert into produtos (id, nome, valor, descricao, quantidade) values (null, ?, ?, ?, ?)', array($nome, $valor, $descricao, $quantidade));
        /*
         * withInput() recupera dados da requisição anterior
         * recupera esses dados na view utilizando old('nome')
         * return redirect("/produtos")->withInput();
         */
        return redirect()
            ->action("ProdutoController@lista")
            ->withInput($request->only("nome"));

    }

}
